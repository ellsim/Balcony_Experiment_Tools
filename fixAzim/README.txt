                                             __
                      ,-_                  (`  ).
                      |-_'-,              (     ).
                      |-_'-'           _(        '`.
             _        |-_'/        .=(`(      .     )
            /;-,_     |-_'        (     (.__.:-`-_.'
           /-.-;,-,___|'          `(       ) )
          /;-;-;-;_;_/|\_ _ _ _ _   ` __.:'   )
             x_( __`|_P_|`-;-;-;,|        `--'
             |\ \    _||   `-;-;-'
             | \`   -_|.      '-'
             | /   /-_| `
             |/   ,'-_|  \
             /____|'-_|___\
      _..,____]__|_\-_'|_[___,.._
     '                          ``'--,..,.      


README - BALCONY DATA CORRECTIONS
Author: Elliot I. Simon <ellsim@dtu.dk>
License: Creative Commons Attribution-ShareAlike 4.0 International (http://creativecommons.org/licenses/by-sa/4.0/)

:INTRO:

Due to errors in the recorded azimuth values within some wind data files,
the campaign data was corrected. The procedure performed is as follows:

:PROCEDURE:

1) Each scenario was catagorised into the following 4 folders:
-Vara East
-Vara West
-Sirocco East
-Sirocco West

2) All 10 minute wind data files from each scenario were merged together
$ for d in ./*/*/* ; do (cd "$d" &&  pwd && cat *.txt > ${PWD##*/}_merged.txt); done
This solved the issue of detemining the beginning azimuth value for each 10 minute file

3) The correct range of azimuth values for each scan type was determined by Guillaume <gule> from the master computer log.
This is stamped in the usual WindScanner format (middle position). The number in brackets is the step.

Sirocco:
                West: 344-256 [-2]
                East: 16-104 [2]
Vara:
                West: 196-284 [2]
                East: 164-76 [-2]


3) A python program was made which reads the merged wind data files and overwrites the azimuth values with the correct range.
It also removes partial scans, if they exist at the end of the scenario files.

The program source <fixazim.py> can be found in the Corrected/PHASE1 folder.
It runs from the terminal with the following arguments:
$ python fixazim.py MERGED_WIND_DATA_FILE_NAME SCAN_TYPE
Where SCAN_TYPE argument = VW, VE, SW, SE (ScannerName+ScanDirection)

The program interactively plots the uncorrected azimuth evolution for each scan as the data is being processed.
It also plots the corrected values for the first 5 scans, and then subsequent multiples of 10 scans as a sanity check.
It then writes out the corrected data with the filename appended with _fixed.
Floating point values are considered to 3 decimal places.

4) The program was run for every merged wind data file in the current folder
For example (bash):

#!/bin/bash
echo "VARA WEST"
cd ./Vara/WEST
for f in *_merged.txt
do
	echo "Processing $f file.. "
	python ~/fixazim/fixazim.py $f VW
done

Or (Windows batch):

for %%i in (Vara\WEST\*_merged.txt) do python fixazim.py "%%i" VW

** Any questions, please contact Elliot <ellsim@dtu.dk> **
