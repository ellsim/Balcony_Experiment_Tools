#!/usr/bin/python

__author__ = 'Elliot I. Simon'
__email__ = 'ellsim@dtu.dk'
__date__ = '27 September, 2016'

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys


def scantype(config):
    return {
        'SW': np.arange(344, 256-2, -2),
        'SE': np.arange(16, 104+2, 2),
        'VW': np.arange(196, 284+2, 2),
        'VE': np.arange(164, 76-2, -2),
    }.get(config, None)


def main():

    pd.options.display.max_rows = 250
    #this one is necessary to avoid floating point errors
    pd.options.display.float_format = '{:.3g}'.format

    file = sys.argv[1]
    if file is None:
        print('Error, scan type is not valid! ')
        sys.exit()

    merged = pd.read_csv(file, sep=';', header=None)
    # print(merged.head(15))
    # print(merged[4].head(100))
    # print(list(merged.columns.values))
    # plt.scatter(merged[2], merged[4])
    # plt.show()

    azim = scantype(sys.argv[2])
    if azim is None:
        print('Error, scan type is not valid! ')
        sys.exit()

    print('using these azimuth values: ')
    print(type(azim))
    print(azim)

    plt.ion()
    x = np.arange(0, 90, 2)

    ax1 = plt.axes()
    line, = plt.plot(x)
    plt.ylim([0, 360])
    scannum = 1

    for x in range(0, len(merged.index), 45):

        scan = merged.iloc[x:x+45, 4]

        if len(azim) == len(scan):
        # this tests for a partial scan at the end of the scenario

            # plt.scatter(x_range, scan)

            # print(scan)
            line.set_xdata(np.arange(len(scan)))
            line.set_ydata(scan)
            title = 'uncorrected ' + 'scannum = ' + str(scannum) + ' / ' + str(int((len(merged.index)/45)))
            ax1.set_title(title)
            plt.draw()
            plt.pause(0.1)

            merged.iloc[x:x+45, 4] = azim

            # plots the corrected values as well (takes longer so only first 5 and every 10 times)
            if scannum <= 5 or scannum % 10 == 0:
                line.set_xdata(np.arange(len(scan)))
                line.set_ydata(merged.iloc[x:x+45, 4])
                title = 'corrected ' + 'scannum = ' + str(scannum) + ' / ' + str(int(len(merged.index)/45))
                ax1.set_title(title)
                plt.draw()
                plt.pause(0.5)

            scannum += 1

            # print('replaced!')
            # print(merged.iloc[x:x+45, 4])

            # if x >= 600:
            #     break

        else:
            print('partial scan detected!')
            print('removing it ')
            merged.drop(merged.index[x:], inplace=True)

    plt.close('all')

    print(' ... writing out corrected file ... ')
    # print(merged.head(3))
    file_out = file[:-4] + '_fixed.txt'
    merged.to_csv(file_out, header=False, sep=';', index=False, float_format='%.3f')


if __name__ == '__main__':
    main()