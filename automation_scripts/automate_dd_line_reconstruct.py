#!/usr/bin/python

__author__ = 'Elliot I. Simon'
__email__ = 'ellsim@dtu.dk'
__date__ = '14 November, 2016'

"""
This program automates the processing of the dd_line_reconstruct.py program.
It determines the matching filenames and passes them as arguments to the reconstruction program.
"""

from os import system, walk
from sys import exit

vara = []
sirocco = []

# populate list of files in VW_fixed directory
# break because the folder is only 1 level deep
for (vdirpath, vdirnames, vfilenames) in walk('VW_fixed\\'):
    vara.extend(vfilenames)
    break

# populate list of files in SW_fixed directory
# break because the folder is only 1 level deep
for (sdirpath, sdirnames, sfilenames) in walk('SW_fixed\\'):
    sirocco.extend(sfilenames)
    break

# check that the number of wind data files is the same within both directories
if len(vara) != len(sirocco):
    print("Number of wind data files does not match.. exiting!".upper())
    exit()

# sort the filenames numerically/alphabetically
vara = sorted(vara)
sirocco = sorted(sirocco)

# execute the dd_line_reconstruct program with files as arguments
# prints the file names and also absolutely value of difference between datetime in name. If difference greater than 100
for i in range(len(vara)):
    print('vara file = ' + vara[i])
    print('sirocco file = ' + sirocco[i])

    drift = abs(int(vara[i].split('_')[0]) - int(sirocco[i].split('_')[0]))
    print('Time delta between systems = {0}'.format(drift))
    if drift > 150:
        print('Error! These scenarios do not match!'.upper())
        exit()

    system('python dd_line_reconstruct.py ' + vdirpath+vara[i] + ' ' + sdirpath+sirocco[i])