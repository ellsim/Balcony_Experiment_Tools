                                             __
                      ,-_                  (`  ).
                      |-_'-,              (     ).
                      |-_'-'           _(        '`.
             _        |-_'/        .=(`(      .     )
            /;-,_     |-_'        (     (.__.:-`-_.'
           /-.-;,-,___|'          `(       ) )
          /;-;-;-;_;_/|\_ _ _ _ _   ` __.:'   )
             x_( __`|_P_|`-;-;-;,|        `--'
             |\ \    _||   `-;-;-'
             | \`   -_|.      '-'
             | /   /-_| `
             |/   ,'-_|  \
             /____|'-_|___\
      _..,____]__|_\-_'|_[___,.._
     '                          ``'--,..,.      


README - BALCONY DUAL DOPPLER CENTRAL LINE RECONSTRUCT
Author: Elliot I. Simon <ellsim@dtu.dk>
License: Creative Commons Attribution-ShareAlike 4.0 International (http://creativecommons.org/licenses/by-sa/4.0/)

:INTRO:

This tool reconstructs vector components (u,v) leading to horizontal wind speed and direction along the space & time synchronised transect line.
The detailed procedure performed is as follows:

:PROCEDURE:

1) The program is run from the console:
	$ python dd_line_reconstruct.py <1> <2>
   Where <1> = the merged, corrected wind data file for scanner 1 (Vara), and <2> = the matching wind data file for scanner 2 (Sirocco)

2) The files passed as arguments are read into memory using pandas read_csv as separate Dataframes

3) Checks are made that neither file is size 0 Bytes, and that the length of both data files are equal

4) Intersecting points (azimuth and range gates from both scanners) were calculated beforehand, and entered into a dictionary variable (points)

5) A new Dataframe (df_tidy) is initialised, which will hold time synchronised data from both scanners

6) Each scan (45 lines) is sliced (looping over the read in files in blocks of this size). Another nested loop recurses through the dictionary of points 
   within each sliced scan

7) For each point within each scan, the starting column number of measurements for the range gate of interest (within the original file) is determined 
   (function get_col_num). The timestamp is also converted from LabView format to DateTime (function convtime)

8) Starting from the correct column, corresponding measurement data from both wind data files is extracted

9) The combined measurements are organised into a logical format, and inserted into df_tidy. See:
   line = [id, scannum, dd1datetime, dd1azim, dd1elev, dd1rg, dd1dtstop, dd1rs, dd1cnr, dd1disp,
                    dd2datetime, dd2azim, dd2elev, dd2rg, dd2dtstop, dd2rs, dd2cnr, dd2disp]
   
10) The column 'timedelta' is calculated, which represents the time drift between the two scanners.
    The user is warned if any rows have a timedelta > 5 seconds

11) Horizontal wind vector components are calculated according to the equations/procedure outlined in [1]
    This is done by applying the functions calc_u and calc_v to the entire df_tidy Dataframe row wise

12) The vector components are combined to give scalar outputs for horizontal wind speed and direction, applying functions 
    calc_hspeed and calc_dir in the same way as the previous step

13) The index of the result Dataframe is set to the DateTime object obtained for scanner #1 in step #7. This enforces that no rows are duplicates,
    and is a smart way to store time series data

14) A basic filter that the CNR (carrier to noise ratio) of both scanners must be > 28 dB is applied

15) String manipulation is done to obtain the file output name, and both the unfiltered and filtered data is written to the disk


[1] Simon, E & Courtney, M 2016, A Comparison of sector-scan and dual Doppler wind measurements at H�vs�re Test Station � one lidar or two? 
    DTU Wind Energy. DTU Wind Energy E, vol. 0112 http://orbit.dtu.dk/files/125285452/RUNE_D1.2_ellsim_final.pdf

** Any questions, please contact Elliot <ellsim@dtu.dk> **
