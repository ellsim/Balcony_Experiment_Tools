#!/usr/bin/python

__author__ = 'Elliot I. Simon'
__email__ = 'ellsim@dtu.dk'
__date__ = '20 October, 2016'

# This program reconstructs dual Doppler synthesis from the Oesterild Balcony experiment
# For the synchronised central transect line

# VARA IS DD1, SIROCCO IS DD2

import pandas as pd
# import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import datetime


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def convtime(labviewtime):
    """
    convert time from LabVIEW epoch (Jan 1, 1904) to UNIX epoch (Jan 1,1970)
    and then to common timestamp (YYYY-MM-DD HH:MM:SS
    """

    unixtime = labviewtime - 2082844800
    timestamp = datetime.datetime.utcfromtimestamp(int(unixtime)).strftime('%Y-%m-%d %H:%M:%S')
    return timestamp


def calc_u(line):

    ur1 = line[7]
    ur2 = line[15]
    theta1 = np.deg2rad(line[3])
    theta2 = np.deg2rad(line[11])

    u = ((ur1 * np.cos(theta2)) - (ur2 * np.cos(theta1))) / ((np.sin(theta1) * np.cos(theta2)) -
                                                             (np.sin(theta2) * np.cos(theta1)))

    # if line[0] == 15:
    #     print(u, ur1, ur2, theta1, theta2)

    return u


def calc_v(line):

    ur1 = line[7]
    ur2 = line[15]
    theta1 = np.deg2rad(line[3])
    theta2 = np.deg2rad(line[11])

    v = ((ur2 * np.sin(theta1)) - (ur1 * np.sin(theta2))) / ((np.sin(theta1) * np.cos(theta2)) -
                                                             (np.sin(theta2) * np.cos(theta1)))
    return v


def calc_hspeed(line):

    u = line[19]
    v = line[20]

    return np.sqrt(np.power(u, 2) + np.power(v, 2))


def calc_dir(line):

    u = line[19]
    v = line[20]

    return np.degrees(np.arctan2(v, u)) + 180


def get_col_num(range_gate):
    """
    Determines the column number to slice for a given range gate.
    Note this conversion is only valid for the Balconies wind data files!!

    Explanation: 1st range gate starts at 105m. They are spaced evenly 35m apart. The wind data files are in sets of
    4 column oriented values (dist, radial speed, cnr, disp). The first set begins at index 6
    (0-5 are local key, dt, azim, elev)

    :param range_gate: range gate from scanner in m
    :return: column number in wind_data file
    """

    colnum = int((((range_gate - 105)/35)*4)+6)

    return colnum


def main():

    pd.options.display.max_rows = 50
    # This is necessary to avoid floating point errors
    pd.options.display.float_format = '{:.3g}'.format

    # Parses command line arguments into filenames
    file1 = sys.argv[1]
    file2 = sys.argv[2]
    # if file1 or file2 is None:
    #     print(bcolors.FAIL + 'Error, file is not valid! ')
    #     sys.exit(1)

    # checks that that neither file has is empty

    if (os.stat(file1).st_size == 0 or os.stat(file2).st_size == 0):
        print(bcolors.FAIL + "Error! One or both files has a size of 0 bytes. Aborting!!".upper())
        sys.exit(1)

    df_dd1 = pd.read_csv(file1, sep=';', header=None)
    df_dd1.rename(columns={3: 'dt_stop', 4: 'azim', 5: 'elev'}, inplace=True)
    df_dd2 = pd.read_csv(file2, sep=';', header=None)
    df_dd2.rename(columns={3: 'dt_stop', 4: 'azim', 5: 'elev'}, inplace=True)

    # Checks that the correct wind data files were loaded (matching lengths). Exit if they do not
    if len(df_dd1.index) != len(df_dd2.index):
        print("Loaded wind data files differ in length! \r")
        print("Please check that the scenarios match ")
        sys.exit(1)

    # range_gates = np.arange(105, 7000+35, 35)

    # Manually determined intersecting points (as close as we can get)
    points = [
            {'n': 0, 'dd1azim': 196, 'dd2azim': 344, 'dd1rg': 2135, 'dd2rg': 2135},
            {'n': 1, 'dd1azim': 198, 'dd2azim': 342, 'dd1rg': 2135, 'dd2rg': 2135},
            {'n': 2, 'dd1azim': 200, 'dd2azim': 340, 'dd1rg': 2170, 'dd2rg': 2170},
            {'n': 3, 'dd1azim': 202, 'dd2azim': 338, 'dd1rg': 2205, 'dd2rg': 2205},
            {'n': 4, 'dd1azim': 204, 'dd2azim': 336, 'dd1rg': 2240, 'dd2rg': 2240},
            {'n': 5, 'dd1azim': 206, 'dd2azim': 334, 'dd1rg': 2275, 'dd2rg': 2275},
            {'n': 6, 'dd1azim': 208, 'dd2azim': 332, 'dd1rg': 2310, 'dd2rg': 2310},
            {'n': 7, 'dd1azim': 210, 'dd2azim': 330, 'dd1rg': 2345, 'dd2rg': 2345},
            {'n': 8, 'dd1azim': 212, 'dd2azim': 328, 'dd1rg': 2415, 'dd2rg': 2415},
            {'n': 9, 'dd1azim': 214, 'dd2azim': 326, 'dd1rg': 2450, 'dd2rg': 2450},
            {'n': 10, 'dd1azim': 216, 'dd2azim': 324, 'dd1rg': 2520, 'dd2rg': 2520},
            {'n': 11, 'dd1azim': 218, 'dd2azim': 322, 'dd1rg': 2590, 'dd2rg': 2590},
            {'n': 12, 'dd1azim': 220, 'dd2azim': 320, 'dd1rg': 2660, 'dd2rg': 2660},
            {'n': 13, 'dd1azim': 222, 'dd2azim': 318, 'dd1rg': 2730, 'dd2rg': 2730},
            {'n': 14, 'dd1azim': 224, 'dd2azim': 316, 'dd1rg': 2835, 'dd2rg': 2835},
            {'n': 15, 'dd1azim': 226, 'dd2azim': 314, 'dd1rg': 2905, 'dd2rg': 2905},
            {'n': 16, 'dd1azim': 228, 'dd2azim': 312, 'dd1rg': 3045, 'dd2rg': 3045},
            {'n': 17, 'dd1azim': 230, 'dd2azim': 310, 'dd1rg': 3150, 'dd2rg': 3150},
            {'n': 18, 'dd1azim': 232, 'dd2azim': 308, 'dd1rg': 3290, 'dd2rg': 3290},
            {'n': 19, 'dd1azim': 234, 'dd2azim': 306, 'dd1rg': 3430, 'dd2rg': 3430},
            {'n': 20, 'dd1azim': 236, 'dd2azim': 304, 'dd1rg': 3605, 'dd2rg': 3605},
            {'n': 21, 'dd1azim': 238, 'dd2azim': 302, 'dd1rg': 3780, 'dd2rg': 3780},
            {'n': 22, 'dd1azim': 240, 'dd2azim': 300, 'dd1rg': 4025, 'dd2rg': 4025},
            {'n': 23, 'dd1azim': 242, 'dd2azim': 298, 'dd1rg': 4270, 'dd2rg': 4270},
            {'n': 24, 'dd1azim': 244, 'dd2azim': 296, 'dd1rg': 4550, 'dd2rg': 4550},
            {'n': 25, 'dd1azim': 246, 'dd2azim': 294, 'dd1rg': 4900, 'dd2rg': 4900},
            {'n': 26, 'dd1azim': 248, 'dd2azim': 292, 'dd1rg': 5285, 'dd2rg': 5285},
            {'n': 27, 'dd1azim': 250, 'dd2azim': 290, 'dd1rg': 5775, 'dd2rg': 5775},
            {'n': 28, 'dd1azim': 252, 'dd2azim': 288, 'dd1rg': 6335, 'dd2rg': 6335}
            ]

    # df_tidy is the dataframe used for collecting all the data in one place
    df_tidy = pd.DataFrame(columns=['id', 'scannum', 'dd1datetime', 'dd1azim', 'dd1elev', 'dd1rg', 'dd1dtstop', 'dd1rs',
                                    'dd1cnr', 'dd1disp', 'dd2datetime', 'dd2azim', 'dd2elev', 'dd2rg', 'dd2dtstop',
                                    'dd2rs', 'dd2cnr', 'dd2disp'], index=np.arange(0, (len(points) * len(df_dd1) / 45)))

    scannum = 0
    localid = 0

    for x in range(0, len(df_dd1), 45):

        sys.stdout.write("\rPlease stand by: processing scannum {0} out of {1} total."
                         .format(scannum, int(len(df_dd1)/45)-1))
        sys.stdout.flush()

        # Slices the wind data by individual scans in the loop
        scan_dd1 = df_dd1.iloc[x:x+45, :]
        scan_dd2 = df_dd2.iloc[x:x+45, :]

        # loops through the dictionary of points
        for i in points:
            # gets the first column number of the range gate we're interested in, since it's a bit tricky
            dd1_col = get_col_num(i['dd1rg'])
            dd2_col = get_col_num(i['dd2rg'])

            # print("point number: ",  i['n'])
            # print("column number (dd1): ", dd1_col)
            # print("column number (dd2): ", dd2_col)

            # extracts the measurement points we want to combine
            dd1_los = scan_dd1[scan_dd1.azim == i['dd1azim']]
            dd2_los = scan_dd2[scan_dd2.azim == i['dd2azim']]

            # print(convtime(dd1_los.iloc[:, 3]))
            # print(dd1_los.iloc[:, [3, 4, 5, dd1_col, dd1_col + 1, dd1_col + 2, dd1_col + 3]])
            # print(dd2_los.iloc[:, [3, 4, 5, dd2_col, dd2_col + 1, dd2_col + 2, dd2_col + 3]])

            # grabs the variable values out of the LOS slice into easier to read names
            id = i['n']
            scannum = scannum
            dd1datetime = convtime(dd1_los.iloc[:, 3])
            dd1azim = dd1_los.iloc[:, 4].values[0]
            dd1elev = dd1_los.iloc[:, 5].values[0]
            dd1rg = dd1_los.iloc[:, dd1_col].values[0]
            dd1dtstop = dd1_los.iloc[:, 3].values[0]
            dd1rs = dd1_los.iloc[:, dd1_col+1].values[0]
            dd1cnr = dd1_los.iloc[:, dd1_col+2].values[0]
            dd1disp = dd1_los.iloc[:, dd1_col+3].values[0]

            dd2datetime = convtime(dd2_los.iloc[:, 3])
            dd2azim = dd2_los.iloc[:, 4].values[0]
            dd2elev = dd2_los.iloc[:, 5].values[0]
            dd2rg = dd2_los.iloc[:, dd2_col].values[0]
            dd2dtstop = dd2_los.iloc[:, 3].values[0]
            dd2rs = dd2_los.iloc[:, dd2_col + 1].values[0]
            dd2cnr = dd2_los.iloc[:, dd2_col + 2].values[0]
            dd2disp = dd2_los.iloc[:, dd2_col + 3].values[0]

            # recombines the values into the final format we want (column wise)
            line = [id, scannum, dd1datetime, dd1azim, dd1elev, dd1rg, dd1dtstop, dd1rs, dd1cnr, dd1disp,
                    dd2datetime, dd2azim, dd2elev, dd2rg, dd2dtstop, dd2rs, dd2cnr, dd2disp]
            # stores the organised values into the df_tidy (working data)
            df_tidy.ix[localid] = line

            localid += 1

        scannum += 1

    # computes timedelta column, which is the difference between the two devices measuring at their respective points
    df_tidy['timedelta'] = np.abs(df_tidy['dd1dtstop'] - df_tidy['dd2dtstop'])

    print("\n")
    print(df_tidy.head(5))

    # checks if any of the points have a time delta of more than 5 seconds. If so, warns the user.
    # otherwise, prints the average time delta of all points
    if df_tidy['timedelta'].values.any() > 5:
        print(bcolors.WARNING + "\nCheck your drift yo! dt_stop between scanners is > 5 seconds.\n".upper() + bcolors.ENDC)
    else:
        print(bcolors.OKGREEN + "OK- Average time drift between devices is: {0}".format(df_tidy['timedelta'].values.mean())
        + bcolors.ENDC)

    # calc u and v components
    df_tidy['u'] = df_tidy.apply(calc_u, axis=1)
    df_tidy['v'] = df_tidy.apply(calc_v, axis=1)

    # calc hspeed and dir
    df_tidy['hspeed'] = df_tidy.apply(calc_hspeed, axis=1)
    df_tidy['dir'] = df_tidy.apply(calc_dir, axis=1)

    # sets the index to dt_stop of Vara (datetime object)
    df_tidy.set_index(df_tidy['dd1datetime'], inplace=True)

    # filters out lines with CNR of < -28 in either scanner's measurement
    df_result_filtered = df_tidy[(df_tidy['dd1cnr'] > -28) & (df_tidy['dd2cnr'] > -28)]
    print(df_result_filtered.head(20))

    # writes out files and prints summary to console
    # grabs the name of the first wind data file provided using some string manipulation
    file_name_append = str(file1).split('\\')[-1].split('.')[0]
    print(' ... writing out files ... ')

    print(df_tidy.head(5))
    print(df_result_filtered.head(5))

    df_tidy.to_csv('.\out\\'+file_name_append+'_all_data.csv', header=True, sep=';', index=True, float_format='%.3f')
    df_result_filtered.to_csv('.\out\\'+file_name_append+'_filtered.csv', header=True, sep=';', index=True, float_format='%.3f')

if __name__ == '__main__':
    main()
