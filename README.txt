                                             __
                      ,-_                  (`  ).
                      |-_'-,              (     ).
                      |-_'-'           _(        '`.
             _        |-_'/        .=(`(      .     )
            /;-,_     |-_'        (     (.__.:-`-_.'
           /-.-;,-,___|'          `(       ) )
          /;-;-;-;_;_/|\_ _ _ _ _   ` __.:'   )
             x_( __`|_P_|`-;-;-;,|        `--'
             |\ \    _||   `-;-;-'
             | \`   -_|.      '-'
             | /   /-_| `
             |/   ,'-_|  \
             /____|'-_|___\
      _..,____]__|_\-_'|_[___,.._
     '                          ``'--,..,.      


README - BALCONY DATA TOOLKIT
Author: Elliot I. Simon <ellsim@dtu.dk>
License: Creative Commons Attribution-ShareAlike 4.0 International (http://creativecommons.org/licenses/by-sa/4.0/)

:INTRO:

This set of tools was developed to process data from the Balconies experiment at �sterild test site.
It includes the following components:

:FOLDER/FILE LIST & DESCRIPTION:


1) fixazim : This tool (fixazim.py) corrects errors in the azimuth values written in wind_data files due to a control loop error of the devices.
   Also included is a detailed README and example data for input/output, as well as a screenshot of the realtime plotting.

2) dd_line_reconstruct : This tool loads the corrected wind_data files from both scanners, and obtains vector components (u,v), leading to horizontal
   wind speed and direction along the synchronised transect line, using the procedure and equations which can be found in [1]. Unfiltered and filtered
   output files are written out. Example input (the output from fixazim.py for both scanners), and output is provided under example_data.
   A detailed README is also included.

3) automation_scripts : This folder includes various scripts (both UNIX bash and Windows batch) which aid in preparing data for the main programs,
   such as merging together the 10 minute wind data files, and automated launching of fixazim.py for each scan type. Only Vara-West scripts are included,
   because the other scans types only differ by the folder names and abbreviations (i.e. VW, VE, SW, SE).
   It also includes automate_dd_line_reconstruct.py, which determines matching filenames for the respective scanner data, ensures they are the same
   length, and launches dd_line_reconstruct with the proper arguments.

[1] Simon, E & Courtney, M 2016, A Comparison of sector-scan and dual Doppler wind measurements at H�vs�re Test Station � one lidar or two? 
    DTU Wind Energy. DTU Wind Energy E, vol. 0112 http://orbit.dtu.dk/files/125285452/RUNE_D1.2_ellsim_final.pdf

** Any questions, please contact Elliot <ellsim@dtu.dk> **